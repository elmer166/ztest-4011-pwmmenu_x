/*! \file 4011-PWMmenu.c
 *
 *  \brief Adjust LED brightness via menu
 *
 * Author: jjmcd
 * Created on September 7, 2012, 8:53 PM
 */

#include <p30Fxxxx.h>
#include <stdio.h>
#include <stdlib.h>
#include "lcd.h"

// Configuration fuses
_FOSC(XT_PLL16 & PRI)                   // 7.3728 rock * 16 = 118MHz
_FWDT(WDT_OFF)                          // Watchdog timer off
_FBORPOR(PWRT_16 & PBOR_OFF & MCLR_EN)  // Brownout off, powerup 16ms
_FGS(GWRP_OFF & CODE_PROT_OFF)          // No code protection

//! Initialize LCD, PWMs, Ports
void Initialize(void);

//! Debounced state of each of the buttons
int nAction[4];
// Currently active LED on the menu
int nMenu;
//! Indicate that LCD must be updated
int nChanged;
//! Duty cycle for each LED to get similar brightness
//int nDutyCycle[3][10] = {{1023, 1000, 998, 995, 991, 986, 960, 860, 760, 248 },
//  {1023, 1000, 998, 995, 991, 986, 960, 880, 800, 512 },
//  {1023, 1000, 998, 995, 991, 986, 960, 512, 256, 0 }};
int nDutyCycle[3][10] = {{1023, 1000, 998, 995, 991, 986, 960, 860, 760, 0 },
  {1023, 1000, 999, 995, 991, 986, 960, 920, 880, 700 },
  {1023, 1000, 998, 995, 991, 986, 960, 800, 700, 512 }};
//! Current brightness level for each LED
int nBrightness[3];

//! Mainline for 4011-PWMmenu
/*! Display a menu, scroll through menu with left and right buttons.
 * Up and down buttons adjust brightness of selected LED
 */
int main(void)
{
    //! Work string for displays
    char szWork[16];
    //! misc counter
    int i;

    // Initialize ports, LCD, etc.
    Initialize();
    // Set each button to already handled
    for ( i=0; i<4; i++ )
        nAction[i] = 0;
    // Set all LEDs at dim
    for ( i=0; i<3; i++ )
        nBrightness[i] = 2;
    // Set current selection to red
    nMenu = 0;
    // Force first display
    nChanged = 1;

    LCDclear();
    while (1)
    {
        // Display the selections
        LCDhome();
        LCDputs(" Red  Yel  Grn ");
        LCDline2();
        // If the menu selection has been changed
        if (nChanged)
        {
            // Clear out line 2
            LCDputs("               ");
            // Point to the selected LED
            LCDposition(0x41+5*nMenu);
            LCDputs("^");
            // Display the setting for each LED
            for ( i=0; i<3; i++ )
              {
                LCDposition(0x43+5*i);
                LCDletter('0'+nBrightness[i]);
              }
            // Remember we updated
            nChanged = 0;
        }
        // Right button pressed?
        if (nAction[1])
        {
            // Move selection right
            nMenu++;
            // If off end, move to extreme left
            if (nMenu > 2)
                nMenu = 0;
            // Remember we did this
            nAction[1] = 0;
            // and note that the LCD needs to be updated
            nChanged = 1;
        }
        // Left button pressed?
        if (nAction[0])
        {
            nMenu--;
            if (nMenu < 0)
                nMenu = 2;
            nAction[0] = 0;
            nChanged = 1;
        }
        // Up button pressed?
        if ( nAction[2])
        {
            // Increase brightness for current LED
            nBrightness[nMenu]++;
            // but don't let it get too big
            if ( nBrightness[nMenu] > 9 )
                nBrightness[nMenu] = 9;
            // Remember we handled this button
            nAction[2] = 0;
            // Display the new duty cycle
            LCDposition( (0x40+16) );
            sprintf(szWork,"%4d",
                    nDutyCycle[nMenu][nBrightness[nMenu]]);
            LCDputs(szWork);
            // Remember to update bottom row
            nChanged = 1;
        }
        // Down button pressed
        if ( nAction[3])
        {
            nBrightness[nMenu]--;
            if ( nBrightness[nMenu] < 0 )
                nBrightness[nMenu] = 0;
            nAction[3] = 0;
            LCDposition( (0x40+16) );
            sprintf(szWork,"%4d",
                    nDutyCycle[nMenu][nBrightness[nMenu]]);
            LCDputs(szWork);
            nChanged = 1;
        }
        // Set LED brightness
        OC2RS = nDutyCycle[0][nBrightness[0]];
        OC4RS = nDutyCycle[1][nBrightness[1]];
        OC3RS = nDutyCycle[2][nBrightness[2]];
    }
}

